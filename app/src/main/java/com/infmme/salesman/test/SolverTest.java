package com.infmme.salesman.test;

import android.test.InstrumentationTestCase;
import com.infmme.salesman.Solver;
import junit.framework.Assert;

/**
 * Created by infm on 1/12/15.
 */
public class SolverTest extends InstrumentationTestCase {

	Solver solver = new Solver();
	Solver.Solution solution;

	public SolverTest(){
		solver.solve(6);
		solution = solver.getSolution();
	}

	public void testIsSearchCompleted(){
		Assert.assertTrue(solution.citiesOrder.size() == 6);
		Assert.assertTrue(solution.pathLength != 0 && solution.pathLength != Float.MAX_VALUE);
	}

	public void testIsTimeOk(){
		Assert.assertTrue(solver.getTimeElapsed() < 1000);
	}

	public void testException(){
		try {
			solver.solve(0);
		} catch (IllegalArgumentException e) {
			assert true;
		}
	}
}
