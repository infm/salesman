package com.infmme.salesman;

import android.util.Pair;

import java.util.*;

/**
 * Created by infm on 1/12/15.
 */
public class Solver {

	private static final int CITIES_NUM = 6;
	private static final List<City> CITIES = Arrays.asList(
			new City(52.5167f, 13.3833f), //Berlin
			new City(48.1333f, 11.5667f), //Munich
			new City(49.0092f, 8.404f),   //Karlsruhe
			new City(51.55f, 6.85f),      //Oberhausen
			new City(52.3667f, 9.7167f),  //Hannover
			new City(51.3167f, 9.5f)      //Kassel
	);
	//cache solutions to reduce unnecessary calculations
	Map<Integer, Solution> cachedSolutions = new TreeMap<Integer, Solution>();
	private long timeElapsed;
	private Solution solution;

	public Solution getSolution(){
		return solution;
	}

	public long getTimeElapsed(){
		return timeElapsed;
	}

	public double getPathLength(){
		return solution.pathLength;
	}

	/**
	 * Method to get cities in order of visiting.
	 *
	 * @return list of pairs (to avoid making City class public).
	 */
	public ArrayList<Pair<Float, Float>> getCitiesOrder(){
		ArrayList<Pair<Float, Float>> list = new ArrayList<Pair<Float, Float>>();
		for (City c : solution.citiesOrder)
			list.add(new Pair<Float, Float>(c.x, c.y));
		return list;
	}

	/**
	 * Main method of the class, solves the problem by brute force.
	 *
	 * @param citiesNum : number of cities for which we should solve the TSP.
	 */
	public void solve(int citiesNum){
		if (citiesNum > CITIES_NUM || citiesNum < 1)
			throw new IllegalArgumentException("Cities number is illegal");

		if (cachedSolutions.containsKey(citiesNum)){
			solution = cachedSolutions.get(citiesNum);
			timeElapsed = 0;
			return;
		}

		boolean used[] = new boolean[citiesNum];
		used[0] = true; //we've already been in this vertex

		Solution s = new Solution();
		City start = CITIES.get(0);
		s.citiesOrder.add(start);

		long startTime = System.currentTimeMillis();
		solution = solutionStep(start, used, citiesNum, s);
		timeElapsed = System.currentTimeMillis() - startTime;
		cachedSolutions.put(citiesNum, solution);
	}

	/**
	 * Helper method of solve() which finds optimal path from vertex.
	 *
	 * @param v         : vertex, from which we seek.
	 * @param used      : helper array to check, if we've been in a city.
	 * @param citiesNum : number of cities, narrowing our search bounds.
	 * @param acc       : accumulator of data. Used when we're deepening into recursion.
	 * @return optimal Solution for v.
	 */
	private Solution solutionStep(City v, boolean[] used, int citiesNum, Solution acc){
		Solution res = new Solution();

		//we have to find minimum and set it to res, so it should be as bigger as possible
		res.pathLength = Float.MAX_VALUE;

		for (int i = 0; i < citiesNum; ++i){
			if (!used[i]){
				used[i] = true; //mark next vertex as visited
				City u = CITIES.get(i);
				Solution candidate = solutionStep(u, used, citiesNum,
												  acc.addCity(getDistance(v, u), u));
				if (candidate.pathLength < res.pathLength) //check if we've improved result
					res = candidate;
				used[i] = false; //mark this vertex as not visited, so we could use it later
			}
		}
		if (res.pathLength == Float.MAX_VALUE) //check if we haven't find any unvisited vertices
			return acc; //return accumulated value
		return res;
	}

	private float getDistance(City a, City b){
		return (float) Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
	}

	private static class City {
		public float x;
		public float y;

		public City(float x, float y){
			this.x = x;
			this.y = y;
		}
	}

	public static final class Solution {
		public float pathLength;
		public List<City> citiesOrder;

		public Solution(){
			pathLength = 0;
			citiesOrder = new ArrayList<City>();
		}

		public Solution addCity(float len, City v){
			Solution res = new Solution();
			res.pathLength = pathLength + len;
			res.citiesOrder = new ArrayList<City>(citiesOrder);
			res.citiesOrder.add(v);
			return res;
		}
	}
}
