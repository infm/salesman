package com.infmme.salesman;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	private TextView statusBar;
	private FrameLayout container;

	private Handler handler;
	private Solver solver;

	private SalesmanView view5;
	private SalesmanView view6;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		container = (FrameLayout) findViewById(R.id.vis_container);
		solver = new Solver();

		handler = new Handler();

		statusBar = (TextView) findViewById(R.id.status_bar);
		findViewById(R.id.button_5_cities).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v){
				new Thread(new Runnable() { //run solver in separate thread to avoid freezing of UI
					@Override
					public void run(){
						solver.solve(5);
						handler.post(new Runnable() {
							@Override
							public void run(){
								displayResult(5);
							}
						});
					}
				}).start();
			}
		});

		findViewById(R.id.button_6_cities).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v){
				new Thread(new Runnable() {
					@Override
					public void run(){
						solver.solve(6);
						handler.post(new Runnable() {
							@Override
							public void run(){
								displayResult(6);
							}
						});
					}
				}).start();
			}
		});
	}

	private void displayResult(int count){
		statusBar.setText(String.format("Length: %.3f Time: %dms", solver.getPathLength(), solver.getTimeElapsed()));
		container.removeAllViews();
		if (count == 5){
			if (view5 == null){
				view5 = new SalesmanView(this, solver.getCitiesOrder().subList(0, count));
				view5.setBackgroundColor(Color.WHITE);
			}
			container.addView(view5);
		} else if (count == 6){
			if (view6 == null){
				view6 = new SalesmanView(this, solver.getCitiesOrder().subList(0, count));
				view6.setBackgroundColor(Color.WHITE);
			}
			container.addView(view6);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		return item.getItemId() == R.id.action_settings || super.onOptionsItemSelected(item);
	}
}
