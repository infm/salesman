package com.infmme.salesman;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Pair;
import android.view.View;

import java.util.List;

/**
 * Created by infm on 1/12/15.
 */
public class SalesmanView extends View {

	private static final int OFFSET_X = 100;
	private static final int OFFSET_Y = 1300;
	private static final int COEFF = 32;
	private static final int VERTEX_RADIUS = 10;

	private Paint paint;
	private List<Pair<Float, Float>> cities;

	public SalesmanView(Context context, List<Pair<Float, Float>> cities){
		super(context);
		paint = new Paint();
		paint.setColor(Color.BLUE);
		this.cities = cities;
	}

	@Override
	protected void onDraw(Canvas canvas){
		int citiesNum = cities.size();
		int height = canvas.getHeight();

		for (int i = 1; i <= citiesNum; ++i){
			Pair<Float, Float> currentCity = cities.get(i % citiesNum);
			Pair<Float, Float> prevCity = cities.get(i - 1);
			float y1 = height - (currentCity.first * COEFF - OFFSET_Y);
			float y2 = height - (prevCity.first * COEFF - OFFSET_Y);

			float x1 = currentCity.second * COEFF - OFFSET_X;
			float x2 = prevCity.second * COEFF - OFFSET_X;

			canvas.drawLine(x1, y1, x2, y2, paint);
			canvas.drawCircle(x1, y1, VERTEX_RADIUS, paint);
		}
	}
}
